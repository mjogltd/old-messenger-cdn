# README #

Messenger Firebase Hosting CDN Repository.

The public URL for this project is 

https://messenger-storage.firebaseapp.com

https://assets.mjog.net



### What is this repository for? ###

* This is a CDN for any static items that are required for the Messenger App or the Micro Apps
- Icons
- Images

### How do I get set up? ###

We have not build a jenkins task yet so manual deploy to Firebase Hosting is required.

1.  Install the Firebase CLI Tools
``` npm install -g firebase-tools ```
2. From the repository folder login to firebase
``` firebase login```
3. Deploy to Firebase
``` firebase deploy```


### Who do I talk to? ###

* John Bayley john.bayley@mjog.com
